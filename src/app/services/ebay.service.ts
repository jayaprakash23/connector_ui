import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constant } from '../constant/constant';
import { EbayAccessToken } from '../models/ebay-access-token';
import { NotifySync } from '../models/notify-sync';

@Injectable({
  providedIn: 'root'
})
export class EbayService {
  constructor(private http: HttpClient) { }

  url = Constant.CONNECTOR + '/' + 'api/';


  getAllClients(Page: number, size: number, sort): Observable<Object> {
    return this.http.get(`${this.url}clientlist/?page=${Page}&size=${size}&sort=${sort}`);
  }

  saveClientDetailsById(client: EbayAccessToken): Observable<Response[]> {
    return this.http.post<Response[]>(`${this.url}add`, client)
  }

  getClientId(id: number): Observable<Object> {
    return this.http.get(`${this.url}client/${id}`);
  }

  saveSynctDetailsById(sync: NotifySync): Observable<Response[]> {
    return this.http.post<Response[]>(`${this.url}product/add/seller`, sync)
  }
}