export class EbayAccessToken {
    id: any;
    clientId: any;
    clientSecret: any;
    refreshToken: any;
    //token save columns
    accessToken: any;
    expiresIn: any;
    tokenType: any;
    refreshTokenExpiresIn: any;
    createdDate: any;
    updatedDate: any;
}
