import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { EbayService } from 'src/app/services/ebay.service';
import { validateAllFormFields } from 'src/app/utils/admin-pipeline-validators';

@Component({
  selector: 'vex-notify-sync',
  templateUrl: './notify-sync.component.html',
  styleUrls: ['./notify-sync.component.scss']
})
export class NotifySyncComponent implements OnInit {

  notifysyncForm: FormGroup;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private ebayService: EbayService,
  ) { }

  ngOnInit() {
    this.createSyncForm();
  }

  createSyncForm() {
    this.notifysyncForm = this.formBuilder.group({
      id: ['', Validators.required],
      platformId: ['', Validators.required],
      collectionId: ['', Validators.required],
    },
    );
  }

  saveSync() {
    if (this.notifysyncForm.valid) {
      this.ebayService.saveSynctDetailsById(this.notifysyncForm.value).subscribe(data => {
        this.snackBar.open('Notify Sync info send Successfully', '×', { panelClass: 'success', verticalPosition: 'bottom', duration: 3000 });
        this.router.navigate(['/clientinfo']);
      });
    }
    else {
      validateAllFormFields(this.notifysyncForm);
    }
  }

}
