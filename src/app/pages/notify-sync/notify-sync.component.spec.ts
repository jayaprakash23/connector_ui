import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifySyncComponent } from './notify-sync.component';

describe('NotifySyncComponent', () => {
  let component: NotifySyncComponent;
  let fixture: ComponentFixture<NotifySyncComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotifySyncComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifySyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
