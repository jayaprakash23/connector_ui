import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotifySyncComponent } from './notify-sync.component';

const routes: Routes = [

  { path: '', component: NotifySyncComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotifySyncRoutingModule { }
