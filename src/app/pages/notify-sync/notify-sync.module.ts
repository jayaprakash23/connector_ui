import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotifySyncRoutingModule } from './notify-sync-routing.module';
import { CommonMaterialModule } from 'src/app/common-material-module';
import { NotifySyncComponent } from './notify-sync.component';


@NgModule({
  declarations: [NotifySyncComponent],
  imports: [
    CommonModule,
    NotifySyncRoutingModule,
    CommonMaterialModule
  ]
})
export class NotifySyncModule { }
