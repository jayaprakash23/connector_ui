import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CustomerCreateUpdateComponent } from './customer-create-update.component';
import { CommonMaterialModule } from 'src/app/common-material-module';

@NgModule({
  imports: [
    CommonModule,
    CommonMaterialModule
  ],
  declarations: [CustomerCreateUpdateComponent],
  entryComponents: [CustomerCreateUpdateComponent],
  exports: [CustomerCreateUpdateComponent]
})
export class CustomerCreateUpdateModule {
}
