import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { validateAllFormFields } from 'src/app/utils/admin-pipeline-validators';
import { EbayService } from 'src/app/services/ebay.service';
import { EbayAccessToken } from 'src/app/models/ebay-access-token';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import icClose from '@iconify/icons-ic/twotone-close';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
@Component({
  selector: 'vex-customer-create-update',
  templateUrl: './customer-create-update.component.html',
  styleUrls: ['./customer-create-update.component.scss']
})
export class CustomerCreateUpdateComponent implements OnInit {
  clientForm: FormGroup;
  id: any;
  all: any;
  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private ebayService: EbayService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    public location: Location,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id']
    this.getClientId(this.id)
    this.createRegisterForm();
    this.createClient()
  }
  icClose = icClose;

  validationMessages = {
    'createdDate': [
      { type: 'required', message: 'Created Date is required' },
    ],
    'updatedDate': [
      { type: 'required', message: 'Updated Date is required' },
    ],
  }

  createRegisterForm() {
    this.clientForm = this.formBuilder.group({
      id: [''],
      clientId: ['', Validators.required],
      clientSecret: ['', Validators.required],
      refreshToken: ['', Validators.required],
      accessToken: ['', Validators.required],
      expiresIn: ['', Validators.required],
      tokenType: ['', Validators.required],
      refreshTokenExpiresIn: ['', Validators.required],
      createdDate: [''],
      updatedDate: [''],

    },
    );
  }

  
  getClientId(id: number) {
    this.ebayService.getClientId(id)
      .subscribe(
        (client: EbayAccessToken) => {
          this.all=client['data']
          this.editClient(this.all);
        },
        error => {
          console.log(error)
        }
      )
  }

  editClient(client: EbayAccessToken) {
    this.clientForm.patchValue({
      id: client.id,
      clientId: client.clientId,
      clientSecret: client.clientSecret,
      refreshToken: client.refreshToken,
      accessToken: client.accessToken,
      expiresIn: client.expiresIn,
      tokenType: client.tokenType,
      refreshTokenExpiresIn: client.refreshTokenExpiresIn,
      createdDate: client.createdDate,
      updatedDate: client.updatedDate
    });
  }


  createClient() {
    if (this.clientForm.valid) {
      this.ebayService.saveClientDetailsById(this.clientForm.value).subscribe(data => {
        this.snackBar.open('Client info saved Successfully', '×', { panelClass: 'success', verticalPosition: 'bottom', duration: 3000 });
        this.router.navigate(['/clientinfo']);
        this.dialog.closeAll();
      });
    }
    else {
      validateAllFormFields(this.clientForm);
    }
  }
  back() {
    this.location.back()
  }

  close() {
    this.router.navigate([`/clientinfo`])

  }

}
