import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListColumn } from '../../../../@vex/interfaces/table-column.interface';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { stagger40ms } from '../../../../@vex/animations/stagger.animation';
import { FormControl } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import { getPageSize } from 'src/app/utils/admin-pipeline-validators';
import { EbayService } from 'src/app/services/ebay.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import icEdit from '@iconify/icons-ic/twotone-edit';
import { BehaviorSubject } from 'rxjs';
import { CustomerCreateUpdateComponent } from './customer-create-update/customer-create-update.component';
import { EbayAccessToken } from 'src/app/models/ebay-access-token';
import { MatDialog } from '@angular/material/dialog';
import { Observable, ReplaySubject } from 'rxjs';
import { Router } from '@angular/router';
import { ErrormsgComponent } from 'src/app/errormsg/errormsg.component';

@UntilDestroy()

@Component({
  selector: 'vex-aio-table',
  templateUrl: './aio-table.component.html',
  styleUrls: ['./aio-table.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class AioTableComponent implements OnInit {
  @Input() type: string;
  layoutCtrl = new FormControl('boxed');
  customers: EbayAccessToken[];
  subject$: ReplaySubject<EbayAccessToken[]> = new ReplaySubject<EbayAccessToken[]>(1);
  data$: Observable<EbayAccessToken[]> = this.subject$.asObservable();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  configSuccess: MatSnackBarConfig = {
    panelClass: 'error-msg',
    horizontalPosition: 'center',
    verticalPosition: 'top'
  };
  clientList: any;
  elements: any;
  pageSizeValue: any;
  sortParam: any = 'id,DESC';
  pageList: any;
  public page: any = 0;
  public size: any = 10;
  filterActive: boolean = false;
  search: any;
  paginationDetail = new BehaviorSubject({
    length: 10,
    pageIndex: 0,
    pageSize: 10
  });

  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icSearch = icSearch;
  icFilterList = icFilterList;
  icAdd = icAdd
  icEdit = icEdit;
  constructor(
    private clientService: EbayService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAllClients()
  }
  editClientId(id) {
    this.router.navigate([`/clientinfo/${id}`])
  }
  getAllClients() {
    this.clientService.getAllClients(this.page, this.size, this.sortParam).subscribe(
      res => {
        this.clientList = new MatTableDataSource();
        this.clientList = res['content'];
        this.elements = res['numberOfElements'];
        this.clientList.sort = this.sort;
        this.clientList.paginator = this.paginator;
        this.pageList = res['totalElements'];
        this.pageSizeValue = getPageSize(this.pageList);
      },
      error => {
        this.errorMsg();
        console.log(error)
      }
    )
  }

  errorMsg() {
    this.snackBar.openFromComponent(ErrormsgComponent, {
      data: 'Service unavailable',
      ...this.configSuccess
    });
  }

  sortData(event) {
    this.sortParam = event.active;
    if (event.direction == 'asc') {
      this.sortParam = this.sortParam + "," + "ASC";
    } else if (event.direction == 'desc') {
      this.sortParam = this.sortParam + "," + "DESC";
    }
  }

  getUpdate(event) {
    this.paginationDetail.next(event);
    this.page = event.pageIndex;
    this.size = event.pageSize;
  }





  @Input()
  columns: ListColumn[] = [
    { name: 'Id', property: 'id', isModelProperty: true, visible: true },
    { name: 'Client Id', property: 'clientId', isModelProperty: true, visible: true },
    { name: 'ClientSecret', property: 'clientSecret', isModelProperty: false, visible: true, },
    { name: 'Refresh Token', property: 'refreshToken', isModelProperty: false, visible: true },
    { name: 'Access Token', property: 'accessToken', isModelProperty: false, visible: true },
    { name: 'ExpiresIn', property: 'expiresIn', isModelProperty: true, visible: true },
    { name: 'TokenType', property: 'tokenType', isModelProperty: true, visible: true },
    { name: 'RefreshToken ExpiresIn', property: 'refreshTokenExpiresIn', isModelProperty: true, visible: true },
    { name: 'CreatedDate', property: 'createdDate', isModelProperty: false, visible: true },
    { name: 'UpdatedDate', property: 'updatedDate', isModelProperty: false, visible: true },
    { name: 'Action', property: 'actions', visible: true, disabled: true },

  ] as ListColumn[];

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  createCustomer() {
    this.dialog.open(CustomerCreateUpdateComponent).afterClosed().subscribe((customer: EbayAccessToken) => {
      /**
       * Customer is the updated customer (if the user pressed Save - otherwise it's null)
       */
      if (customer) {
        /**
         * Here we are updating our local array.
         * You would probably make an HTTP request here.
         */
       // this.customers.unshift(new EbayAccessToken(customer));
        this.subject$.next(this.customers);
      }
    });
  }

}
