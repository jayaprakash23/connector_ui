import { trigger, transition, style, animate } from '@angular/animations';
import { FormGroup } from '@angular/forms';

//this method using validata all inputs columns
export function validateAllFormFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    control.markAsTouched({ onlySelf: true });
  });
}

//this method using page size count based
export function getPageSize(size) {
  if (size >= 10) {
    if (size > 10) {
      return [5, 10, 15];
    }
    else if (size <= 10 && size > 5) {
      return [5, 10];
    }
  }
  else if (size <= 10 && size > 5) {
    return [5, 10];
  }
  else {
    return null;
  }

}
export let fadeInAnimation = trigger('fadeInAnimation', [
  transition('void => *', [
    style({
      opacity: 0,
    }),
    animate('400ms 150ms ease-in-out', style({
      opacity: 1,
    }))
  ]),
]);