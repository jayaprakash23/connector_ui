import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipelineTrimDirective } from './pipeline-trim.directive';


@NgModule({
  declarations: [PipelineTrimDirective],
  imports: [
    CommonModule
  ],
  exports: [PipelineTrimDirective]
})
export class DirectiveModule { }
