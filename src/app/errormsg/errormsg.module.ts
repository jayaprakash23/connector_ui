import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrormsgComponent } from './errormsg.component';
import { CommonMaterialModule } from 'src/app/common-material-module';

@NgModule({
  declarations: [ErrormsgComponent],
  imports: [
    CommonModule,
    CommonMaterialModule
  ],
  entryComponents: [
    ErrormsgComponent
  ],
})
export class ErrormsgModule { }
