import { Component, Inject, OnInit } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'vex-errormsg',
  templateUrl: './errormsg.component.html',
  styleUrls: ['./errormsg.component.scss']
})
export class ErrormsgComponent implements OnInit {

  constructor(
    public snackBarRef: MatSnackBarRef<ErrormsgComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

}
