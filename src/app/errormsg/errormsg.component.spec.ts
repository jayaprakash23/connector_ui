import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ErrormsgComponent } from './errormsg.component';

describe('ErrormsgComponent', () => {
  let component: ErrormsgComponent;
  let fixture: ComponentFixture<ErrormsgComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrormsgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrormsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
