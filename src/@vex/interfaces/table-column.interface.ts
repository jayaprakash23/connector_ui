export interface TableColumn<T> {
  label: string;
  property: keyof T | string;
  type: 'text' | 'image' | 'badge' | 'progress' | 'checkbox' | 'button';
  visible?: boolean;
  cssClasses?: string[];
}
export class ListColumn {
  name?: string;
  property?: string;
  visible?: boolean;
  isModelProperty?: boolean;
  displayFn: any;
  disabled?: boolean;
  hide?: boolean
  method?: any;
}
